import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianList from "./TechnicianList";
import TechnicianForm from "./TechnicianForm";
import AppointmentList from "./AppointmentList";
import AppointmentHistory from "./AppointmentHistory";
import AppointmentForm from "./AppointmentForm";
import SalespersonList from "./SalespersonList";
import AddSalesperson from "./AddSalesperson";
import Customers from "./Customer";
import AddCustomerForm from "./AddCustomer";
import Sales from "./sales";
import SalesForm from "./SalesForm";
import SalespersonHistory from "./salespersonhistory";
import ManufacturerList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import ModelList from "./ModelList";
import ModelForm from "./ModelForm";
import AutoList from "./AutoList";
import AutoForm from "./AutoForm";

function App(props) {
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [appointmentHistory, setAppointmentHistory] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [autos, setAutos] = useState([]);

  async function getManufacturers() {
    try {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      const { manufacturers } = await response.json();
      setManufacturers(manufacturers);
    } catch (err) {
      console.error(err);
    }
  }

  async function getModels() {
    try {
      const response = await fetch("http://localhost:8100/api/models/");
      const { models } = await response.json();
      setModels(models);
    } catch (err) {
      console.error(err);
    }
  }

  async function getAutos() {
    try {
      const response = await fetch("http://localhost:8100/api/automobiles/");
      const { autos } = await response.json();
      setAutos(autos);
    } catch (err) {
      console.error(err);
    }
  }

  async function getTechnicians() {
    const response = await fetch("http://localhost:8080/api/technicians/");
    try {
      const response = await fetch("http://localhost:8080/api/technicians/");
      const { technicians } = await response.json();
      setTechnicians(technicians);
    } catch (err) {
      console.error(err);
    }
  }

  async function getAppointments() {
    try {
      const response = await fetch("http://localhost:8080/api/appointments/");
      const { appointments } = await response.json();
      setAppointments(appointments);
    } catch (err) {
      console.error(err);
    }
  }

  async function getAppointmentHistory() {
    try {
      const response = await fetch("http://localhost:8080/api/appointments/");
      const { appointments } = await response.json();
      setAppointmentHistory(appointments);
    } catch (err) {
      console.error(err);
    }
  }

  useEffect(() => {
    getTechnicians();
    getAppointments();
    getAppointmentHistory();
    getManufacturers();
    getModels();
    getAutos();
  }, []);

  if (technicians === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="manufacturers">
          <Route
            index
            element={<ManufacturerList manufacturers={manufacturers} />}
          />
          <Route
            path="new"
            element={
              <ManufacturerForm
                manufacturers={manufacturers}
                getManufacturers={getManufacturers}
              />
            }
          />
        </Route>
        <Route path="models">
          <Route index element={<ModelList models={models} />} />
          <Route
            path="new"
            element={<ModelForm models={models} getModels={getModels} />}
          />
        </Route>
        <Route path="autos">
          <Route
            index
            element={<AutoList autos={autos} getAutos={getAutos} />}
          />
          <Route
            path="new"
            element={<AutoForm autos={autos} getAutos={getAutos} />}
          />
        </Route>
        <Route path="technicians">
          <Route index element={<TechnicianList technicians={technicians} />} />
          <Route
            path="new"
            element={
              <TechnicianForm
                technicians={technicians}
                getTechnicians={getTechnicians}
              />
            }
          />
        </Route>
        <Route path="appointments">
          <Route
            index
            element={
              <AppointmentList
                appointments={appointments}
                getAppointments={getAppointments}
              />
            }
          />
          <Route
            path="new"
            element={
              <AppointmentForm
                technicians={technicians}
                getAppointments={getAppointments}
              />
            }
          />
        </Route>
        <Route path="appointmentHistory">
          <Route
            index
            element={
              <AppointmentHistory
                appointmentHistory={appointmentHistory}
                getAppointmentHistory={getAppointmentHistory}
              />
            }
          />
        </Route>
        <Route path="/salespeople" element={<SalespersonList />} />
        <Route path="/salespeople/create" element={<AddSalesperson />} />
        <Route path="/customers" element={<Customers />} />
        <Route path="/customers/create" element={<AddCustomerForm />} />
        <Route path="/sales" element={<Sales />} />
        <Route path="/sales/create" element={<SalesForm />} />
        <Route path="/salespeople/history" element={<SalespersonHistory />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
