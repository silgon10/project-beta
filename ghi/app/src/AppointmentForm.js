import React, { useState, useEffect } from "react";

function AppointmentForm(props) {
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [reason, setReason] = useState("");
  const [vin, setVIN] = useState("");
  const [customer, setCustomer] = useState("");
  const [technician, setTechnician] = useState("");
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8080/api/technicians/")
      .then((response) => response.json())
      .then((data) => {
        setTechnicians(data.technicians);
      })
      .catch((err) => console.log(err));
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const date_time = new Date(date + "T" + time + "Z").toISOString();

    const appointment = {
      date_time,
      reason,
      vin,
      customer,
      technician,
    };

    const response = await fetch("http://localhost:8080/api/appointments/", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(appointment),
    });

    if (response.ok) {
      setDate("");
      setTime("");
      setReason("");
      setCustomer("");
      setVIN("");
      setTechnician("");
      props.getAppointments();
    }
  };

  function handleChangeDate(event) {
    const { value } = event.target;
    setDate(value);
  }

  function handleChangeTime(event) {
    const { value } = event.target;
    setTime(value);
  }

  function handleChangeReason(event) {
    const { value } = event.target;
    setReason(value);
  }

  function handleChangeCustomer(event) {
    const { value } = event.target;
    setCustomer(value);
  }

  function handleChangeVIN(event) {
    const { value } = event.target;
    setVIN(value);
  }

  function handleChangeTechnician(event) {
    const { value } = event.target;
    setTechnician(value);
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-appointment-form">
                <h1 className="card-title">Make an Appointment</h1>
                <div className="form-floating mb-3">
                  <input
                    value={date}
                    onChange={handleChangeDate}
                    required
                    type="date"
                    id="date"
                    className="form-control"
                  />
                  <label htmlFor="date">Date</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={time}
                    onChange={handleChangeTime}
                    required
                    type="time"
                    id="time"
                    className="form-control"
                  />
                  <label htmlFor="time">Time</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={reason}
                    onChange={handleChangeReason}
                    required
                    placeholder="Reason"
                    type="text"
                    id="reason"
                    name="reason"
                    className="form-control"
                  />
                  <label htmlFor="reason">Reason</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={customer}
                    onChange={handleChangeCustomer}
                    required
                    placeholder="Customer Name"
                    type="text"
                    id="customer"
                    name="customer"
                    className="form-control"
                  />
                  <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={vin}
                    onChange={handleChangeVIN}
                    required
                    placeholder="VIN"
                    type="text"
                    id="vin"
                    name="vin"
                    className="form-control"
                  />
                  <label htmlFor="vin">VIN</label>
                </div>
                <div className="form-floating mb-3">
                  <select
                    value={technician}
                    onChange={handleChangeTechnician}
                    required
                    className="form-select"
                    id="technician"
                    name="technician"
                  >
                    <option value="">Select a technician</option>
                    {technicians.map((technician) => (
                      <option key={technician.id} value={technician.id}>
                        {technician.employee_id}
                      </option>
                    ))}
                  </select>
                  <label htmlFor="technician">Technician</label>
                </div>
                <button className="btn btn-lg btn-primary">Add</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  //     <div className="my-5 container">
  //         <form onSubmit={handleSubmit} id="create-appointment-form">
  //             <h1 className="card-title">Add an Appointment</h1>
  //             <div className="row">
  //                 <div className="col">
  //                     <div className="form-floating mb-3">
  //                         <input value={appointment.date} onChange={handleChange} type="date" id="date" name="date" className="form-control" />
  //                         <input value={appointment.time} onChange={handleChange} type="time" id="time" name="time" className="form-control" />
  //                         <input value={appointment.reason} onChange={handleChange} placeholder="Reason" type="text" id="reason" name="reason" className="form-control" />
  //                         <input value={appointment.vin} onChange={handleChange} placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
  //                         <input value={appointment.customer} onChange={handleChange} placeholder="Customer" type="text" id="customer" name="customer" className="form-control" />
  //                     </div>
  //                 </div>
  //                 <div className="col">
  // <div className="form-floating mb-3">
  //     <select value={appointment.technician} onChange={handleChange} required className="form-select" id="technician" name="technician">
  //         <option value="">Select a technician</option>
  //         {props.technicians.map(tech => (
  //             <option key={tech.id} value={tech.id}>{tech.name}</option>
  //         ))}
  //     </select>
  // </div>
  //                 </div>
  //             </div>
  //             <button className="btn btn-lg btn-primary">Add</button>
  //         </form>
  //     </div>
  // );
}

export default AppointmentForm;
