import React, { useState, useEffect } from "react";

function AppointmentHistory(props) {
    const [appointmentHistory, setAppointmentHistory] = useState([]);
    const [searchValue, setSearchValue] = useState('');

    useEffect(() => {
        async function getApptsDetails() {
            // fetch data
            try {
                const requests = [];
                const appts = [];
                props.appointmentHistory.forEach((appt) => {
                    requests.push(
                        fetch(`http://localhost:8080/api/appointment/${appt.id}/`).then(
                            async (resp) => {
                                const appt = await resp.json();
                                appts.push(appt);
                            }
                        )
                    );
                });
                await Promise.all(requests);
                setAppointmentHistory(appts);
            } catch (err) {
                console.error(err);
            }
        }

        getApptsDetails();
    }, [props.appointmentHistory]);

    const handleSearch = (event) => {
        setSearchValue(event.target.value);
    };

    const filteredAppointments = appointmentHistory.filter(appt =>
        appt.vin.includes(searchValue)
    );

    return (
        <div>
            <h2>Service History</h2>
            <div className="my-3">
                <input
                    type="search"
                    value={searchValue}
                    onChange={handleSearch}
                    className="form-control"
                    placeholder="Search by VIN number"
                />
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments.map((appointment) => {
                        const appoinmentDate = new Date(appointment.date_time);
                        const formattedDate = appoinmentDate.toLocaleDateString();
                        const formattedTime = appoinmentDate.toLocaleTimeString();
                        return (
                            <tr key={appointment.vin}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.vip ? "Yes" : "No"}</td>
                                <td>{appointment.customer}</td>
                                <td>{formattedDate}</td>
                                <td>{formattedTime}</td>
                                <td>{appointment.technician.employee_id}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentHistory;
