import React, { useState, useEffect } from 'react';

function AutoForm(props) {
    const [vin, setVIN] = useState('')
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [model_id, setModel] = useState('')
    const [models, setModels] = useState([])


    useEffect(() => {

        fetch('http://localhost:8100/api/models/')
            .then(response => response.json())
            .then(data => {
                setModels(data.models);
            })
            .catch(err => console.log(err));
    }, []);


    const handleSubmit = async e => {
        e.preventDefault();

        const model = {
            vin,
            color,
            year,
            model_id,
        };

        const response = await fetch('http://localhost:8100/api/automobiles/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(model)
        });

        if (response.ok) {
            setVIN('');
            setColor('');
            setYear('');
            setModel('');
            props.getAutos();
        }
    }

    function handleChangeVIN(event) {
        const { value } = event.target;
        setVIN(value);
    }

    function handleChangeColor(event) {
        const { value } = event.target;
        setColor(value);
    }

    function handleChangeYear(event) {
        const { value } = event.target;
        setYear(Number(value));
    }

    function handleChangeModel(event) {
        const { value } = event.target;
        setModel(value);
    }


    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-auto-form">
                                <h1 className="card-title">Add a Auto</h1>
                                <div className="form-floating mb-3">
                                    <input value={vin} onChange={handleChangeVIN} required type="text" id="vin" className="form-control" />
                                    <label htmlFor="vin">VIN Number</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={color} onChange={handleChangeColor} required type="text" id="color" className="form-control" />
                                    <label htmlFor="color">Color</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={year} onChange={handleChangeYear} required type="text" id="year" className="form-control" />
                                    <label htmlFor="year">Year</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <select value={model_id} onChange={handleChangeModel} required className="form-select" id="model" name="model">
                                        <option value="">Select a Model</option>
                                        {models.map(model => (
                                            <option key={model.id} value={model.id}>{model.name}</option>
                                        ))}
                                    </select>
                                    <label htmlFor="manufacturer">Select Model</label>
                                </div>
                                <button className="btn btn-lg btn-primary">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AutoForm;
