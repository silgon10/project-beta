function AutoList(props) {

    // const [unsoldAutos, setUnsoldAutos] = useState([]);

    // useEffect(() => {
    //     getUnsoldAutos();
    // }, []);

    // async function getUnsoldAutos() {
    //     try {
    //         const initialResponse = await fetch('http://localhost:8100/api/automobiles/');
    //         const { autos: allAutos } = await initialResponse.json();

    //         const detailedAutosPromises = allAutos.map(async (auto) => {
    //             const detailResponse = await fetch(`http://localhost:8100${auto.href}`);
    //             const detailedAuto = await detailResponse.json();
    //             return detailedAuto;
    //         });

    //         const detailedAutos = await Promise.all(detailedAutosPromises);

    //         const unsoldAutos = detailedAutos.filter(auto => auto.sold === false);

    //         setUnsoldAutos(unsoldAutos);
    //     } catch (err) {
    //         console.error(err);
    //     }
    // }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {props.autos.map(auto => {
                    return (
                        <tr key={auto.vin}>
                            <td>{auto.vin}</td>
                            <td>{auto.color}</td>
                            <td>{auto.year}</td>
                            <td>{auto.model.name}</td>
                            <td>{auto.model.manufacturer.name}</td>
                            <td>{auto.sold ? 'Yes' : 'No'}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default AutoList;
