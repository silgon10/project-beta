import React, { useState, useEffect } from 'react';

function ModelForm(props) {
    const [name, setName] = useState('')
    const [picture_url, setPictureURL] = useState('')
    const [manufacturer_id, setManufacturer] = useState('')
    const [manufacturers, setManufacturers] = useState([])


    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(response => response.json())
            .then(data => {
                setManufacturers(data.manufacturers);
            })
            .catch(err => console.log(err));
    }, []);


    const handleSubmit = async e => {
        e.preventDefault();

        const model = {
            name,
            picture_url,
            manufacturer_id,
        };

        const response = await fetch('http://localhost:8100/api/models/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(model)
        });

        if (response.ok) {
            setName('');
            setPictureURL('');
            setManufacturer('');
            props.getModels();
        }
    }

    function handleChangename(event) {
        const { value } = event.target;
        setName(value);
    }

    function handleChangePictureURL(event) {
        const { value } = event.target;
        setPictureURL(value);
    }

    function handleChangeManufacturer(event) {
        const { value } = event.target;
        setManufacturer(value);
    }



    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-model-form">
                                <h1 className="card-title">Add a Model</h1>
                                <div className="form-floating mb-3">
                                    <input value={name} onChange={handleChangename} required type="text" id="name" className="form-control" />
                                    <label htmlFor="name">Model Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={picture_url} onChange={handleChangePictureURL} required type="text" id="picture_url" className="form-control" />
                                    <label htmlFor="picture_url">Picture URL</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <select value={manufacturer_id} onChange={handleChangeManufacturer} required className="form-select" id="manufacturer" name="manufacturer">
                                        <option value="">Select a Manufacturer</option>
                                        {manufacturers.map(manufacturer => (
                                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                        ))}
                                    </select>
                                    <label htmlFor="manufacturer">Select Manufacturer</label>
                                </div>
                                <button className="btn btn-lg btn-primary">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ModelForm;
