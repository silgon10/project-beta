function ModelList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model List</th>
                </tr>
            </thead>
            <tbody>
                {props.models.map(model => {
                    return (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td><img src={model.picture_url} alt={model.name} style={{ width: "100px", height: "auto" }} /></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ModelList;