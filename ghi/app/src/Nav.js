import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <table>
            <tbody>
              <tr className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/"
                  >
                    Home
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/manufacturers/"
                  >
                    Manufacturer List
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/manufacturers/new"
                  >
                    New Manufacturer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/models/"
                  >
                    Model List
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/models/new"
                  >
                    New Model
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/autos/"
                  >
                    Automobile List
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/autos/new"
                  >
                    New Automobile
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/technicians/"
                  >
                    Technician List
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/technicians/new"
                  >
                    New Technician
                  </NavLink>
                </li>
              </tr>
              <tr className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/appointments/"
                  >
                    Appointment List
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/appointments/new"
                  >
                    New Appointment
                  </NavLink>
                </li>

                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    aria-current="page"
                    to="/appointmentHistory/"
                  >
                    Appointment History
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople">
                    Salespeople
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/create">
                    Add Salesperson
                  </NavLink>
                </li>

                <li className="nav-item">
                  <NavLink className="nav-link" to="/customers/">
                    Customers
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customers/create">
                    Add Customer
                  </NavLink>
                </li>

                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales">
                    Sales
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/create">
                    Record Sale
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/history">
                    Salesperson History
                  </NavLink>
                </li>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
