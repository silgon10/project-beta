from common.json import ModelEncoder

from .models import Salesperson, Sale, Customer, AutomobileVO


class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "salesperson",
        "customer",
        "automobile",
    ]

    encoders = {
        'salesperson': SalesPersonEncoder(),
        'customer': CustomerEncoder(),
        'automobile': AutomobileVOEncoder(),
    }
