from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin=models.CharField(max_length=200)
    sold=models.BooleanField(default=False)

class Technician(models.Model):
    first_name=models.CharField(max_length=100)
    last_name=models.CharField(max_length=100)
    employee_id=models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class Appointment(models.Model):
    OPEN = 'open'
    CANCELLED = 'cancelled'
    FINISHED = 'finished'

    STATUS_CHOICES = [
        (OPEN, 'Open'),
        (CANCELLED, 'Cancelled'),
        (FINISHED, 'Finished'),
    ]

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=500)
    status = models.CharField(max_length=200, choices=STATUS_CHOICES, default=OPEN)
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
