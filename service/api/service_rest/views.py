from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
    ]


class AppoinmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "status",
        "vin",
        "customer",
    ]

    def get_extra_data(self, o):
        vin_exists_and_sold = AutomobileVO.objects.filter(
            vin=o.vin, sold=True).exists()
        return {"vip": vin_exists_and_sold}



class AppoinmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "status",
        "vin",
        "customer",
        "vip",
        "date_time",
        "reason",
        "technician",
    ]

    encoders = {
        "technician": TechnicianListEncoder(),
    }

    # check to see if vin of appt maches vin of AutomobileVO to mark as vip or not
    def get_extra_data(self, o):
        vin_exists_and_sold = AutomobileVO.objects.filter(vin=o.vin, sold=True).exists()
        return {"vip": vin_exists_and_sold}


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )


@require_http_methods(["GET"])
def api_show_technician(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppoinmentListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            id = int(content["technician"])
            technician = Technician.objects.get(id=id)
            content["technician"] = technician

        except Technician.DoesNotExist as e:
            print(e)
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400
            )

        try:
            appoinment = Appointment.objects.create(**content)
            return JsonResponse(
                appoinment,
                encoder=AppoinmentDetailEncoder,
                safe=False
            )

        except Exception as e:
            print(e)
            return JsonResponse(
                {"message": "An error occured. Check server logs for more information"},
                status=400
            )


@require_http_methods(["GET", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppoinmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppoinmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "cancelled"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppoinmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "finished"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppoinmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["PUT"])
def api_open_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "open"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppoinmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
